import sys, os, ast, json, time
import pika
import numpy as np


def main():
    # Create connection
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()
    channel.queue_declare(queue='rand')
    channel.queue_declare(queue='solution')

    # Create running windowd
    global running_window
    running_window = np.full(100, -np.inf).astype('int64')

    def callback(ch, method, properties, body):
        # Update running_window 
        body = ast.literal_eval(body.decode('utf-8'))

        global running_max, running_window
        running_window = np.append(running_window, body['rand'])[1:]
        running_max = running_window.max()

        payload = {
            'sequence_number': body['sequence_number'],
            'rand': body['rand'],
            'running_max': int(running_max)
        }
        # Send response
        channel.basic_publish(
            exchange='', routing_key='solution', body=json.dumps(payload)
        )

    channel.basic_consume(queue='rand', on_message_callback=callback, auto_ack=True)
    channel.start_consuming()


if __name__ == '__main__':
    time.sleep(5)
    main()